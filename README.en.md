# SIG的共建共享

#### Description
sig-minzuchess 建立之初即立下宏愿，而实现之唯有将SIG建设成一支有生力量，为开源社区贡献自己。建立这个仓是群策群力，汇集谏言良策共同实现目标的场所，成果就是SIG-minzuchess的社区治理方略。让我们畅所欲言吧。SIG各地区的负责人可以FORK本仓，讨论各地区特有的方略，也可形成自己的意见后，汇集成册，直接采纳。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
