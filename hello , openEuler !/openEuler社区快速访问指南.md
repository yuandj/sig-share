### openEuler社区加入引导指南
 :link: https://gitee.com/openeuler/community/blob/master/zh/contributors/README.md

### 社区规范集合

####  openEuler社区软件包打包规范
 :link: https://gitee.com/openeuler/community/blob/master/zh/contributors/packaging.md

####  openEuler 软件包管理策略原则
 :link: https://gitee.com/openeuler/community/blob/master/zh/technical-committee/governance/software-management.md

####  openEuler社区安全问题处理和发布流程
 :link: https://gitee.com/openeuler/security-committee/blob/master/security-process.md

####  openEuler社区安全漏洞评分规则
 :link: https://gitee.com/openeuler/security-committee/blob/master/security-evaluation.md

####  openEuler社区保护分支锁库指导
 :link: https://gitee.com/openeuler/release-management/blob/master/openEuler%E7%89%88%E6%9C%AC%E9%94%81%E5%BA%93%E6%8C%87%E5%AF%BC.md

####  openEuler社区版本分支代码管理规范
 :link: https://gitee.com/openeuler/release-management/blob/master/openEuler%E7%89%88%E6%9C%AC%E5%88%86%E6%94%AF%E7%BB%B4%E6%8A%A4%E8%A7%84%E8%8C%83.md

####  openEuler社区版本发布评审流程规范
 :link: https://gitee.com/openeuler/release-management/blob/master/openEuler%E7%89%88%E6%9C%AC%E5%8F%91%E5%B8%83%E4%BC%9A%E8%AE%AE%E8%AF%84%E5%AE%A1%E8%A7%84%E8%8C%83.md

### 社区发布件获取路径汇总

####  openEuler社区每日构建版本二进制文件获取地址
 :link: http://117.78.1.88:82/dailybuilds/openeuler/  

#### openEuler社区OBS编译工程访问地址
 :link: https://build.openeuler.org/

#### openEuler社区Jenkins构建ISO镜像工程访问地址
 :link: https://ci.openeuler.org
