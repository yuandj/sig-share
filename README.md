# SIG的共建共享

#### 介绍
[sig-minzuchess](https://gitee.com/yuandj/community/tree/master/sig/sig-minzuchess) 建立之初即立下宏愿，而实现之唯有将SIG建设成一支有生力量，为开源社区贡献自己。建立这个仓是群策群力，汇集谏言良策共同实现目标的场所，成果就是SIG-minzuchess的社区治理方略。让我们畅所欲言吧。SIG各地区的负责人可以FORK本仓，讨论各地区特有的方略，也可形成自己的意见后，汇集成册，直接采纳。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
